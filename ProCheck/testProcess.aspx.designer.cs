﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird. 
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace ProCheck {
    
    
    public partial class TestProcess {
        
        /// <summary>
        /// tb_User-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_User;
        
        /// <summary>
        /// GV_testRuns-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GV_testRuns;
        
        /// <summary>
        /// SDS_openTestRun-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_openTestRun;
        
        /// <summary>
        /// gv_testCase-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gv_testCase;
        
        /// <summary>
        /// SDS_testCases-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_testCases;
        
        /// <summary>
        /// DV_testCases-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DetailsView DV_testCases;
        
        /// <summary>
        /// SDS_testCaseDetail-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_testCaseDetail;
        
        /// <summary>
        /// tb_result-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_result;
        
        /// <summary>
        /// DDL_resultState-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDL_resultState;
        
        /// <summary>
        /// SDS_ResultValues-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_ResultValues;
        
        /// <summary>
        /// B_saveResult-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button B_saveResult;
        
        /// <summary>
        /// GV_resultHistory-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GV_resultHistory;
        
        /// <summary>
        /// SDS_textResultHistory-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_textResultHistory;
    }
}
