﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;

namespace ProCheck
{
    public partial class TestProcess : System.Web.UI.Page
    {
        bsql xbsql = new bsql();
        auth xauth = new auth();
        Timer xtimer = new Timer();
        protected void Page_Load(object sender, EventArgs e)
        {

            tb_User.Text = xauth.getNT();
            /*
         string hasid =   xbsql.sqlstatementget(string.Format("select id from v_userroles where logon='{0}' and role like 'Test%'", xauth.getNT()));
            if (hasid == "")
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"Sie haben leider keine Berechtigung\")</SCRIPT>");
            //    System.Threading.Thread.Sleep(1000);
          
           //           Response.Redirect("~/home.aspx", false);
            }
             * */
        }
        public void updategrid()
        {
            GV_resultHistory.DataBind();
            gv_testCase.DataBind();
            GV_testRuns.DataBind();
        }
      

        protected void B_saveResult_Click(object sender, EventArgs e)
        {
            if (DDL_resultState.SelectedValue == "Bitte wählen")
            { }
            else
            {
                xbsql.sqlstatement(string.Format("insert into testResult (insert_ts,insert_user,id_testCase,id_testRun,result,status) values ('{0}','{1}',{2},{3},'{4}','{5}')", DateTime.Now.ToString(), xauth.getNT(), gv_testCase.SelectedDataKey.Value,GV_testRuns.SelectedDataKey.Value, tb_result.Text, DDL_resultState.Text));
                updategrid();
                tb_result.Text = "";
                DDL_resultState.SelectedIndex = 0;
            }
        }

        protected void GV_testRuns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "finish":
                    GridViewRow SelectedRow = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    var selid = SelectedRow.Cells[2].Text;

                    xbsql.sqlstatement(string.Format("update testRun set status='finsh', change_user='{1}',change_ts='{2}',ended='{2}' where id={0} ", selid, xauth.getNT(), DateTime.Now.ToString()));
            //        xbsql.sqlstatement(string.Format("insert into issueComment (insert_ts, insert_user,id_issue,description) values ('{0}','{1}',{2},'{3}')",
             //      DateTime.Now.ToString(), xauth.getNT(), selid, xauth.getNT() + " restart Issue "));

                    updategrid();
                    break;
                default:

                    GV_testRuns.SelectedRowStyle.BackColor = System.Drawing.Color.Yellow;
                    break;
            }
            
        }

        protected void gv_testCase_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            gv_testCase.SelectedRowStyle.BackColor = System.Drawing.Color.Yellow;
        }

      
    }
}