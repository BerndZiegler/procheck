﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;


namespace saus2
{
    public class bmail
    {
        public String sendeMail(String An, String headertext, String htmltext)
        {
            try
            {
                if (ConfigurationManager.AppSettings["sendmail"] == "yes")
                {
                    try
                    {
                    htmltext = Regex.Replace(htmltext, @"\r\n?|\n", "<br>");
                    var mailempfaenger = An.Split(';');
                    System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                    //AlternateView alternate = AlternateView.CreateAlternateViewFromString(htmltext, mimeType);
                    //  MailMessage message = new MailMessage(ConfigurationManager.AppSettings["emailabsender"], An, headertext,htmltext);
                    MailMessage message = new MailMessage();

                    htmltext.Replace("'", " ");

                    headertext.Replace("'", " ");
                    message.From = new MailAddress((ConfigurationManager.AppSettings["emailabsender"]));
                    message.Subject = headertext;
                    message.Body = htmltext;
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    message.IsBodyHtml = true;

                    foreach (string xmailadress in mailempfaenger)
                    {
                        if (xmailadress != "")
                            message.To.Add(xmailadress);
                    }
                    // message.CC.Add(isauth.getInfo("email"));
                    // message.AlternateViews.Add(alternate);
                    SmtpClient emailclient = new SmtpClient(ConfigurationManager.AppSettings["smtp"]);

                        emailclient.Send(message);
                        return "Mail erfolgreich versendet";
                    }
                    catch (Exception ex)
                    {
                        return ("error: " + ex.Message);
                    }
                }
            }
            catch
            {
                throw;
            }
            return "shouldsendnomail";
            }
        public String sendeMail(String An, String headertext, String htmltext, String CC, String Absender)
        {
            try
            {
                if (ConfigurationManager.AppSettings["sendmail"] == "yes")
                {
                    System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                    AlternateView alternate = AlternateView.CreateAlternateViewFromString(htmltext, mimeType);
                    MailMessage message = new MailMessage(Absender, An, headertext,
                      htmltext);
                    message.CC.Add(CC);
                    message.AlternateViews.Add(alternate);
                    SmtpClient emailclient = new SmtpClient(ConfigurationManager.AppSettings["smtp"]);
                    try
                    {
                        emailclient.Send(message);
                        return "Mail erfolgreich versendet";
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                }
            }
            catch
            {
                throw;
            }
                return "shouldsendnomail";
           
        }
        public String sendeMail(String An, String headertext, String htmltext, String Absender)
        {
            try
            {

                if (ConfigurationManager.AppSettings["sendmail"] == "yes")
                {
                    System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                    AlternateView alternate = AlternateView.CreateAlternateViewFromString(htmltext, mimeType);
                    MailMessage message = new MailMessage(Absender, An, headertext,
                      htmltext);

                    message.AlternateViews.Add(alternate);
                    SmtpClient emailclient = new SmtpClient(ConfigurationManager.AppSettings["smtp"]);
                    try
                    {
                        emailclient.Send(message);
                        return "Mail erfolgreich versendet";
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                }
            }
            catch
            {
                throw;
            }
            return "shouldsendnomail";
           
        }

        public String sendeMailBulk(DataSet An, String headertext, String htmltext, String Absender)
        {
            try
            {

                System.Net.Mime.ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                AlternateView alternate = AlternateView.CreateAlternateViewFromString(htmltext, mimeType);

                foreach (DataRow row in An.Tables["email"].Rows)
                {
                    //    {
                    //        xbmail.sendeMail(row["email"].ToString(), "Es wurde ein neuer Eintrag in CoCi gemacht.", "","noreply@oce.com");
                    //   }

                    MailMessage message = new MailMessage(Absender, row["email"].ToString(), headertext,
                      htmltext);

                    message.AlternateViews.Add(alternate);
                    SmtpClient emailclient = new SmtpClient(ConfigurationManager.AppSettings["smtp"]);
                    try
                    {
                        emailclient.Send(message);
                        //  return "Mail erfolgreich versendet";
                    }
                    catch (Exception ex)
                    {
                        //  return ex.ToString();
                        throw ex;
                    }
                }
            }
            catch
            {
                throw;
            }
            return "Fertig";
        }
    }
}