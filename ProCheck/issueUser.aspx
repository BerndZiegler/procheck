﻿<%@ Page Title=""  Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="issueUser.aspx.cs" Inherits="ProCheck.issueUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <hr />
        <h1>Eigene Issues verwalten
            <asp:TextBox ID="tb_User" Visible="false" runat="server"></asp:TextBox>
        </h1>
    <hr />
    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="Button1_Click"><h2>Neuen Issue erstellen</h2></asp:LinkButton>
       
        <asp:Panel ID="PanelNeuerIssue" runat="server" Visible="False">
            <table >
                <tr>
                    <td>Installation:</td>
                    <td>
                        <asp:DropDownList ID="DDL_Instance" runat="server" DataSourceID="SDS_Instance" DataTextField="ort" DataValueField="Id">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SDS_Instance" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" 
                        SelectCommand="select i.id, i.site + ' ' + i.project as ort from instance i "
               
                        >
                        <FilterParameters>
                            <asp:ControlParameter ControlID="tb_User" Name="status" PropertyName="Text" ConvertEmptyStringToNull="false" Type="String" />
                        </FilterParameters>

                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>Typ:</td>
                    <td>
                        <asp:DropDownList ID="DDL_Type" runat="server" DataSourceID="SDS_common" DataTextField="value" DataValueField="value">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SDS_common" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT [value] FROM [common] WHERE ([cluster] = @cluster)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="IssueType" Name="cluster" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>Titel: </td>
                    <td>
                        <asp:TextBox ID="tb_titel" Width="100%" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Beschreibung:</td>
                    <td>
                        <asp:TextBox ID="tb_description" runat="server" Height="150px" TextMode="MultiLine" Width="800px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Priorität</td>
                    <td>
                        <asp:DropDownList ID="DDL_Prio" runat="server" DataSourceID="SDS_IssuePrio" DataTextField="value" DataValueField="value">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="SDS_IssuePrio" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT [value] FROM [common] WHERE ([cluster] = @cluster)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="IssuePrio" Name="cluster" Type="String" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
            </table>
            
&nbsp;<asp:Button ID="b_IssueSave" runat="server" OnClick="b_IssueSave_Click" Text="Neuen Issue anlegen" />
            
        </asp:Panel>
    <hr />
        <h2>Offene Issues</h2>
        <p>
            <asp:GridView ID="GV_OpenIssues" runat="server" AllowPaging="True" AllowSorting="True" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_openIssues" OnRowCommand="GV_OpenIssues_RowCommand">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                    <asp:BoundField DataField="insert_ts" HeaderText="Startzeit" SortExpression="insert_ts" />
                    <asp:BoundField DataField="insert_user" HeaderText="Ersteller" SortExpression="insert_user" />
                    <asp:BoundField DataField="titel" HeaderText="Titel" SortExpression="titel" />
                    <asp:TemplateField HeaderText="Beschreibung" SortExpression="description">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("description") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%#  Eval("description").ToString().Replace("\n", "<br />") %>'></asp:Label>
                        </ItemTemplate>
                       
                    </asp:TemplateField>
                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                    <asp:BoundField DataField="prio" HeaderText="Prio" SortExpression="prio" />
                    <asp:BoundField DataField="type" HeaderText="Typ" SortExpression="type" />
                    <asp:BoundField DataField="ort" HeaderText="Ort" SortExpression="ort" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SDS_openIssues" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT issue.id, issue.insert_ts, issue.insert_user, issue.change_ts, issue.change_user, issue.id_instance, issue.type, issue.titel, issue.description, issue.status, issue.prio, issue.endtime, instance.site + ' ' + instance.project AS ort FROM issue INNER JOIN instance ON issue.id_instance = instance.Id WHERE (issue.status &lt;&gt; 'cancel') AND (issue.status &lt;&gt; 'solved') ORDER BY issue.id DESC">
            </asp:SqlDataSource>
        </p><hr />
           
    <asp:LinkButton ID="lb_NewRemark" runat="server" OnClick="lb_NewRemark_Click"> <h2>Neue Anmerkung eintragen</h2></asp:LinkButton>
        <asp:Panel ID="P_NewRemark" runat="server" Visible="false">
    <table id="t_newRemark"><tr><td>
            <asp:TextBox ID="tb_Comment" runat="server" Height="107px" TextMode="MultiLine" Width="600px"></asp:TextBox> 
        </td><td>          <asp:RadioButtonList ID="RBL_info" runat="server" >
                <asp:ListItem Selected="True" Value="comment">Kommentar</asp:ListItem>
                <asp:ListItem Value="solved">gelöst</asp:ListItem>
                <asp:ListItem Value="cancel">abgebrochen</asp:ListItem>
                <asp:ListItem Value="waitForUser">Nachricht an  Ersteller</asp:ListItem>
                <asp:ListItem Value="develop">Nachricht an Entwicklung</asp:ListItem>
        </asp:RadioButtonList></td> <td>
            <asp:Button ID="b_coment" runat="server" OnClick="b_coment_Click" Text="Anmerkung eintragen" />
        </td>
        </tr></table>
            
        </asp:Panel>
            <h2>Anmerkungen</h2>        
            <asp:GridView ID="GV_Comments" runat="server" AutoGenerateColumns="False" CssClass="Grid" AlternatingRowStyle-CssClass="alt" DataKeyNames="id" DataSourceID="SDS_IssueRemarks" >
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField DataField="insert_ts" HeaderText="Wann" SortExpression="insert_ts" />
                    <asp:BoundField DataField="insert_user" HeaderText="Wer" SortExpression="insert_user" />
                    <asp:TemplateField HeaderText="Anmerkung" SortExpression="description">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("description") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("description").ToString().Replace("\n", "<br />") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Keine Anmkerungen vorhanden
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:SqlDataSource ID="SDS_IssueRemarks" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT * FROM [issueComment] WHERE ([id_issue] = @id_issue) order by id desc">
                <SelectParameters>
                    <asp:ControlParameter ControlID="GV_OpenIssues" Name="id_issue" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
    <hr />
        <h2>geschlossen Issues
         
        </h2>
        <p>Suche (nach Titel, Beschreibung):
            <asp:TextBox ID="tb_search" runat="server" ></asp:TextBox>
&nbsp;<asp:Button ID="b_searchClosed" runat="server" CausesValidation="False" OnClick="b_searchClosed_Click" Text="ok" />
         
        </p>
          <asp:GridView ID="GV_closedIssues" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CssClass="Grid" AlternatingRowStyle-CssClass="alt" DataKeyNames="id" DataSourceID="SDS_closedIssues" OnRowCommand="GV_closedIssues_RowCommand">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
              <Columns>
                  <asp:CommandField ShowSelectButton="True"  />
                    <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnclick" runat="server" CommandName="restart">restart</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                  <asp:BoundField DataField="insert_ts" HeaderText="Gestartet am" SortExpression="insert_ts" />
                  <asp:BoundField DataField="endtime" HeaderText="Beendet am" SortExpression="endtime" />
                  <asp:BoundField DataField="insert_user" HeaderText="Ersteller" SortExpression="insert_user" />
                  <asp:BoundField DataField="titel" HeaderText="Titel" SortExpression="titel" />
                  <asp:TemplateField HeaderText="Beschreibung" SortExpression="description">
                      <EditItemTemplate>
                          <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("description") %>'></asp:TextBox>
                      </EditItemTemplate>
                      <ItemTemplate>
                          <asp:Label ID="Label1" runat="server" Text='<%# Eval("description").ToString().Replace("\n", "<br />") %>'></asp:Label>
                      </ItemTemplate>
                  </asp:TemplateField>
                  <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status" />
                  <asp:BoundField DataField="prio" HeaderText="Prio" SortExpression="prio" />
                  <asp:BoundField DataField="type" HeaderText="Typ" SortExpression="type" />
                  <asp:BoundField DataField="ort" HeaderText="Ort" SortExpression="ort" />
              </Columns>
        </asp:GridView>
            <asp:SqlDataSource ID="SDS_closedIssues" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT issue.id, issue.insert_ts, issue.insert_user, issue.change_ts, issue.change_user, issue.id_instance, issue.type, issue.titel, issue.description, issue.status, issue.prio, issue.endtime, instance.site + ' ' + instance.project AS ort FROM issue INNER JOIN instance ON issue.id_instance = instance.Id WHERE (issue.status = 'cancel') OR (issue.status = 'solved') ORDER BY issue.id DESC">
             
            </asp:SqlDataSource>
        
     <h2>Anmerkungen        
        </h2>
        <asp:GridView ID="gv_closedIssuesRemark" runat="server" AutoGenerateColumns="False" DataKeyNames="id" CssClass="Grid" AlternatingRowStyle-CssClass="alt" DataSourceID="SDS_closedIssuesRemark">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="insert_ts" HeaderText="Wann" SortExpression="insert_ts" />
                <asp:BoundField DataField="insert_user" HeaderText="Wer" SortExpression="insert_user" />
                <asp:TemplateField HeaderText="Anmerkung" SortExpression="description">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("description") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("description").ToString().Replace("\n", "<br />") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SDS_closedIssuesRemark" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT * FROM [issueComment] WHERE ([id_issue] = @id_issue) ORDER BY [id] DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_closedIssues" Name="id_issue" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <br />
        <br />
        
       

</asp:Content>
