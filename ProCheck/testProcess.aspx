﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="testProcess.aspx.cs" Inherits="ProCheck.TestProcess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content  ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1"  runat="server">

    <h1>Testdurchführung</h1><asp:TextBox ID="tb_User" Visible="false" runat="server"></asp:TextBox>
    <div id="testlauf" >
        <hr />
        <h2>1. Testlauf wählen</h2>
        <asp:GridView ID="GV_testRuns" DataKeyNames="Id" runat="server" DataSourceID="SDS_openTestRun" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnRowCommand="GV_testRuns_RowCommand" AutoGenerateColumns="False">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                  <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnclick" runat="server" CommandName="finish">abschließen</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Id" HeaderText="lfd Nr" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                <asp:BoundField DataField="instance" HeaderText="Kunde / Projekt" ReadOnly="True" SortExpression="instance" />
                <asp:BoundField DataField="name" HeaderText="Name" SortExpression="name" />
            </Columns>
            <PagerTemplate>
                Testlauf
            </PagerTemplate>
        </asp:GridView>
        <asp:SqlDataSource ID="SDS_openTestRun" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT tr.Id, ur.site + ' ' + ur.project AS instance, tr.name FROM testRun AS tr INNER JOIN v_userroles AS ur ON tr.id_instance = ur.idinstance WHERE (ur.role = 'Testuser') AND (tr.ended IS NULL) AND (ur.logon = @logon)">
            <SelectParameters>
                <asp:ControlParameter ControlID="tb_User" Name="logon" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <div>
        <hr />
        <h2>2. Testfall wählen</h2>
        <asp:GridView ID="gv_testCase" runat="server" DataKeyNames="id" DataSourceID="SDS_testCases" AutoGenerateColumns="False" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnRowCommand="gv_testCase_RowCommand" AllowPaging="True" AllowSorting="True"  > 
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="titel" HeaderText="Titel" SortExpression="titel" />
                <asp:BoundField DataField="cluster" HeaderText="Gruppe" SortExpression="cluster" />
                <asp:BoundField DataField="description" HeaderText="Beschreibung des Testverlaufes" SortExpression="description" />
                <asp:BoundField DataField="result" HeaderText="Fehlerklasse" SortExpression="status" />
                <asp:BoundField DataField="insert_ts" HeaderText="Zeitstempel" SortExpression="insert_ts" />
                <asp:BoundField DataField="insert_user" HeaderText="Bearbeiter" SortExpression="insert_user" />
                <asp:BoundField DataField="change_ts" HeaderText="geänderter Zeitstempel" SortExpression="change_ts" />
                <asp:BoundField DataField="change_user" HeaderText="Bearbeiter " SortExpression="change_user" />
            </Columns>
            <EmptyDataTemplate>
                Kein Testlauf gewählt oder kein Testcase vorhanden
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:SqlDataSource ID="SDS_testCases" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT id, insert_ts, insert_user, change_ts, change_user, titel, cluster, description, expect_result, ISNULL((SELECT TOP (1) status FROM testResult WHERE (id_testCase = testCase.id) AND (id_testRun = @idtestRun) ORDER BY id DESC), 'offen') AS result FROM testCase WHERE (id IN (SELECT id_testCase FROM testCase2testRun WHERE (id_testRun = @idtestRun)))">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_testRuns" Name="idtestRun" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    
        <h2>Testfalldetail zu gewählten Testfall</h2>
        <asp:DetailsView ID="DV_testCases" runat="server" Height="50px" Width="498px" AutoGenerateRows="False" DataKeyNames="id" DataSourceID="SDS_testCaseDetail" CssClass="Grid" AlternatingRowStyle-CssClass="alt">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Fields>
                <asp:BoundField DataField="id" HeaderText="lfd Nr Testfall" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="titel" HeaderText="Titel" SortExpression="titel" />
                <asp:BoundField DataField="cluster" HeaderText="Gruppe" SortExpression="cluster" />
                <asp:BoundField DataField="description" HeaderText="Beschreibung des Testverlaufes" SortExpression="description" />
                <asp:BoundField DataField="expect_result" HeaderText="Erwartetes Ergebnis SOLL" SortExpression="expect_result" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SDS_testCaseDetail" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT * FROM [testCase] WHERE ([id] = @id)">
            <SelectParameters>
                <asp:ControlParameter ControlID="gv_testCase" Name="id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <div>
        <hr />
        <h2>3. Testergebnis eintragen</h2>
        <asp:TextBox ID="tb_result" runat="server" Height="80px" Width="629px" TextMode="MultiLine"></asp:TextBox>
        <br />
        Status
        <br />
        <asp:DropDownList ID="DDL_resultState" runat="server" DataSourceID="SDS_ResultValues" DataTextField="value" DataValueField="value">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SDS_ResultValues" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT * FROM [common] WHERE ([cluster] = @cluster)">
            <SelectParameters>
                <asp:Parameter DefaultValue="ResultValue" Name="cluster" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br /> 
        <asp:Button ID="B_saveResult" runat="server" OnClick="B_saveResult_Click" Text="Ergebnis speichern" />
    </div>
    <div>
        <hr />
        <h2>Historie der Testergebnisse zu diesem Testfall</h2>
        <asp:GridView ID="GV_resultHistory" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_textResultHistory" CssClass="Grid" AlternatingRowStyle-CssClass="alt">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="lfd Nr Ergebnis" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="insert_ts" HeaderText="Zeitstempel" SortExpression="insert_ts" />
                <asp:BoundField DataField="insert_user" HeaderText="Bearbeiter" SortExpression="insert_user" />
                <asp:BoundField DataField="id_testCase" HeaderText="lfd Nr Testcase" SortExpression="id_testCase" />
                <asp:BoundField DataField="result" HeaderText="Testergebnis" SortExpression="result" />
                <asp:BoundField DataField="status" HeaderText="status" SortExpression="status" />
            </Columns>
            <EmptyDataTemplate>
                Keine vorhergehenden Ergebnisse vorhanden
            </EmptyDataTemplate>
        </asp:GridView>
        <asp:SqlDataSource ID="SDS_textResultHistory" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT id, id_testCase, result, status, insert_ts, insert_user FROM testResult WHERE (id_testCase = @id_testCase and id_testRun=@id_testRun) ORDER BY id DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="gv_testCase" Name="id_testCase" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="GV_testRuns" Name="id_testRun" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>         
    </div>
    </asp:Content>
