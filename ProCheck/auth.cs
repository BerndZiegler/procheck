﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;

namespace saus2
{
      
    public class auth
    {
        bsql xbsql = new bsql();
        string connstr = System.Configuration.ConfigurationManager.ConnectionStrings["ProCheckConnectionString1"].ConnectionString;
             
        public Boolean isAuth(string zugriffslevel)
        {
            Boolean antwort = false;
            try
            {
                SqlConnection con = new SqlConnection(connstr);
                string strSQL = string.Format("Select count(*) from v_userroles where logon like  '%{0}%' and role = '{1}'",getNT().ToString(),zugriffslevel);
                SqlCommand cmd = new SqlCommand(strSQL, con);
                con.Open();
                int anzahl = Convert.ToInt32(cmd.ExecuteScalar().ToString());               
                con.Close();
                if (anzahl > 0)
                    antwort = true;
               
            }
            catch (Exception e)
            {
                antwort = false;
                throw (e);
            }

            return antwort;
        }
       
        public String getInfo(String was)
        {
            string antwort;
            try
            {
                SqlConnection con = new SqlConnection(connstr);
                string httpuser=HttpContext.Current.User.Identity.Name.ToString();

                string strSQL = "Select *  from [dbo].users where logon = '" + httpuser + "'";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                antwort = dr[was].ToString();
                con.Close();
            }
            catch
            {
                antwort = "Fehler: " + was;
            }

            return antwort;
        }

        public String getNT()
        {
            string antwort;
            try
            {

             //   antwort = HttpContext.Current.User.Identity.Name.ToString();
                antwort = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }
            catch
            {
                antwort = "Fehler";
            }

            return antwort;
        }
        public String getUserInfo(String Logon,String Wert)
        {
            string antwort;
            try
            {
               SqlConnection con = new SqlConnection(connstr);
                string httpuser = HttpContext.Current.User.Identity.Name.ToString();

                // string strSQL = "Select *  from [dbo].v_Techniker_allekenntnisse where id = '" + was + "'";
                string strSQL = string.Format("Select {0} from users where logon='{1}'",Wert,Logon);
                SqlCommand cmd = new SqlCommand(strSQL, con);
                con.Open();
                antwort = Convert.ToString(cmd.ExecuteScalar());              
                con.Close();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return antwort;
        }


        public int getUserID()
        {
            string antwort;
            try
            {
                string httpuser = HttpContext.Current.User.Identity.Name.ToString();
                if (httpuser == "")
                    httpuser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                antwort = xbsql.sqlstatementget("Select id  from users where logon = '" + httpuser + "'");
                if (antwort=="")
                {
                    xbsql.sqlstatement(string.Format("insert into users (username,email,logon) values ('{0}','','{0}')", httpuser));
                    antwort = xbsql.sqlstatementget(string.Format("select id from users where username='{0}' and email='' and logon='{0}'", httpuser));
                }
               
            }
            catch
            {
                antwort = "0" ;
            }

            return Convert.ToInt32(antwort);
        }
    }
}
