﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="ProCheck.info" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Informationen zu ProCheck</h1>
    <hr />
    <h2>Inhalt:<br />Testlauf durchführen<br />Neuen Issue erstellen<br />Issue bearbeiten<br />Neuen Issue erstellen</h2>
    <hr />
    <h3>Testlauf durchführen</h3>
    <hr />
    <h3>Themen (Issues) melden</h3>
    
    In ProCheck können Fehler (Incidents), Änderungs (Change), und Erweiterungswünsche gemeldet werden (Feature Requests)
    <h3>Neuen Issue erstellen</h3>
    <ul>
        <li>"Issue melden" wählen</li>
        <li>"Neu" wählen -> Eingabemaske erscheint</li>
        <li>In der Eingabemaske</li>
        <li>die entsprechende Installation wählen</li>
        <li>Den Typ wählen<br />
             Change Request -> Änderungswunsch einer bestehenden Funktion<br />
            Feature Request -> Wunsch einer Funktionserweiterung<br />
            Incident -> Ein Fehler, der behoben werden soll<br /></li>
        <li>einen kurzen Titel angeben, der den Inhalt kurz beschreibt</li>
        <li>einen komplette Beschreibung angeben mit möglichst allen Informationen um Rückfragen zu vermeiden</li>
        <li>Priorität setzen<br />
        low -> wenig Business Impact bedeutet wenig Einfluss auf das Geschäftsergebnis wenig mehraufwand<br />
        medium -> mittleres Business Impact bedeutet mittleren Einfluss -> z.b. SLA könnte nicht eingehalten werden. Zusatzaufwand geht in Tagen<br />
        high -> mittleres Business Impact bedeutet mittleren Einfluss -> z.b. SLA könnte nicht eingehalten werden. Zusatzaufwand geht in Wochen<br /></li>
        <li>"Neuen Issue anlegen" betätigen.</li>
   
    </ul>
    Es wird daraufhin eine Mail an Support und Ersteller erzeugt und der Issue ist unter "Offene Issues" Sichtbar<br />
    <hr />

</asp:Content>
