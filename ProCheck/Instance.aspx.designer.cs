﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird. 
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace ProCheck {
    
    
    public partial class Instance {
        
        /// <summary>
        /// b_insert-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button b_insert;
        
        /// <summary>
        /// gv_instance-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gv_instance;
        
        /// <summary>
        /// SqlDataSource1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SqlDataSource1;
        
        /// <summary>
        /// Master-Eigenschaft
        /// </summary>
        /// <remarks>
        /// Automatisch generierte Eigenschaft
        /// </remarks>
        public new ProCheck.Site1 Master {
            get {
                return ((ProCheck.Site1)(base.Master));
            }
        }
    }
}
