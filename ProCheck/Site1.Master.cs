﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;

namespace ProCheck
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            l_name.Text = xauth.getNT();
            
            if(!xauth.isAuth("Testadmin"))
                for (int i = 0; i < Menu1.Items.Count; i++)
                {
                if (Menu1.Items[i].Value == "Testadmin")
                    {
                        Menu1.Items.Remove(Menu1.Items[i]);
                    }
                }
                if(!xauth.isAuth("Admin"))
                for (int i = 0; i < Menu1.Items.Count; i++)
                {
                if (Menu1.Items[i].Value == "Admin")
                    {
                        Menu1.Items.Remove(Menu1.Items[i]);
                    }
                }
                l_version.Text = Version();
        }
        private string Version()
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

    }
}