﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Instance.aspx.cs" Inherits="ProCheck.Instance" %>
<%@ MasterType VirtualPath="~/Site1.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Instancen verwalten</h1>
    <h2>1. Instanz hinzufügen</h2>
     <asp:Button ID="b_insert" runat="server" Text="Neue Instanz einfügen" OnClick="b_insert_Click" />
    <hr />
    <h2>2. Instanz bearbeiten und Site + Projekt bestimmen</h2>
    <asp:GridView ID="gv_instance" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="Es sind keine Datensätze zum Anzeigen vorhanden." AllowPaging="True" AllowSorting="True">
    <Columns>
        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
        <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
        <asp:BoundField DataField="insert_ts" HeaderText="insert_ts" SortExpression="insert_ts" />
        <asp:BoundField DataField="insert_user" HeaderText="insert_user" SortExpression="insert_user" />
        <asp:BoundField DataField="change_ts" HeaderText="change_ts" SortExpression="change_ts" />
        <asp:BoundField DataField="change_user" HeaderText="change_user" SortExpression="change_user" />
        <asp:BoundField DataField="site" HeaderText="site" SortExpression="site" />
        <asp:BoundField DataField="project" HeaderText="project" SortExpression="project" />
    </Columns>
</asp:GridView>
   
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" DeleteCommand="DELETE FROM [instance] WHERE [Id] = @Id" InsertCommand="INSERT INTO [instance] ([Id], [insert_ts], [insert_user], [change_ts], [change_user], [site], [project]) VALUES (@Id, @insert_ts, @insert_user, @change_ts, @change_user, @site, @project)" ProviderName="<%$ ConnectionStrings:ProCheckConnectionString1.ProviderName %>" SelectCommand="SELECT [Id], [insert_ts], [insert_user], [change_ts], [change_user], [site], [project] FROM [instance]" UpdateCommand="UPDATE [instance] SET [insert_ts] = @insert_ts, [insert_user] = @insert_user, [change_ts] = @change_ts, [change_user] = @change_user, [site] = @site, [project] = @project WHERE [Id] = @Id">
    <DeleteParameters>
        <asp:Parameter Name="Id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="Id" Type="Int32" />
        <asp:Parameter Name="insert_ts" Type="DateTime" />
        <asp:Parameter Name="insert_user" Type="String" />
        <asp:Parameter Name="change_ts" Type="DateTime" />
        <asp:Parameter Name="change_user" Type="String" />
        <asp:Parameter Name="site" Type="String" />
        <asp:Parameter Name="project" Type="String" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="insert_ts" Type="DateTime" />
        <asp:Parameter Name="insert_user" Type="String" />
        <asp:Parameter Name="change_ts" Type="DateTime" />
        <asp:Parameter Name="change_user" Type="String" />
        <asp:Parameter Name="site" Type="String" />
        <asp:Parameter Name="project" Type="String" />
        <asp:Parameter Name="Id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
</asp:Content>
