﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;

namespace ProCheck
{
    public partial class users : System.Web.UI.Page
    {
        bsql xbsql = new bsql();
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void updategrid()
        {
            gv_users.DataBind();
            gv_roles2user.DataBind();
            DDL_roles.DataBind();
        }

        protected void b_neuerUser_Click(object sender, EventArgs e)
        {
            xbsql.sqlstatement(string.Format("insert into users (insert_ts, insert_user) values ('{0}','{1}')", DateTime.Now.ToString(), xauth.getNT()));
            updategrid();
 
        }

        protected void gv_users_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void b_role2user_Click(object sender, EventArgs e)
        {
            try
            {
                int userid = Convert.ToInt32(gv_users.SelectedValue.ToString());
                xbsql.sqlstatement(string.Format("insert into user2roles (id_users,id_roles,id_instance) values ({0},{1},{2})", userid, DDL_roles.SelectedItem.Value,DDL_Instance.SelectedItem.Value));

            }
            catch (Exception ex)
            {
                Response.Write("<script type=\"text/javascript\">alert('Fehler: " + ex.Message + "');</script>");
            }
            updategrid();
        }
    }
}