﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="users.aspx.cs" Inherits="ProCheck.users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Userverwaltung</h1>
    <hr />
    <h2>1. Neuen User anlegen</h2>    
    <asp:Button ID="b_neuerUser" runat="server" Text="Neuer User" OnClick="b_neuerUser_Click" />
    <hr />
    <h2>2. User Bearbeiten oder für Berechtigungszuordnung wählen</h2>
    
    <asp:GridView ID="gv_users" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" EmptyDataText="Es sind keine Datensätze zum Anzeigen vorhanden." OnRowCommand="gv_users_RowCommand">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="insert_ts" HeaderText="insert_ts" SortExpression="insert_ts" />
            <asp:BoundField DataField="insert_user" HeaderText="insert_user" SortExpression="insert_user" />
            <asp:BoundField DataField="change_ts" HeaderText="change_ts" SortExpression="change_ts" />
            <asp:BoundField DataField="change_user" HeaderText="change_user" SortExpression="change_user" />
            <asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
            <asp:BoundField DataField="logon" HeaderText="logon" SortExpression="logon" />
            <asp:BoundField DataField="userrights" HeaderText="userrights" SortExpression="userrights" />
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
        </Columns>
    </asp:GridView>
    <br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" DeleteCommand="DELETE FROM [users] WHERE [id] = @original_id" InsertCommand="INSERT INTO [users] ([insert_ts], [insert_user], [change_ts], [change_user], [username], [logon], [userrights], [email]) VALUES (@insert_ts, @insert_user, @change_ts, @change_user, @username, @logon, @userrights, @email)" SelectCommand="SELECT * FROM [users]" UpdateCommand="UPDATE [users] SET [insert_ts] = @insert_ts, [insert_user] = @insert_user, [change_ts] = @change_ts, [change_user] = @change_user, [username] = @username, [logon] = @logon, [userrights] = @userrights, [email] = @email WHERE [id] = @original_id" OldValuesParameterFormatString="original_{0}">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="insert_ts" Type="DateTime" />
            <asp:Parameter Name="insert_user" Type="String" />
            <asp:Parameter Name="change_ts" Type="DateTime" />
            <asp:Parameter Name="change_user" Type="String" />
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="logon" Type="String" />
            <asp:Parameter Name="userrights" Type="String" />
            <asp:Parameter Name="email" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="insert_ts" Type="DateTime" />
            <asp:Parameter Name="insert_user" Type="String" />
            <asp:Parameter Name="change_ts" Type="DateTime" />
            <asp:Parameter Name="change_user" Type="String" />
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="logon" Type="String" />
            <asp:Parameter Name="userrights" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="original_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <h2>gewählten User Instanz und Berechtigung zuweisen</h2>
    <asp:DropDownList ID="DDL_roles" runat="server" DataSourceID="SDS_roles" DataTextField="role" DataValueField="id">
    </asp:DropDownList>
&nbsp;<asp:DropDownList ID="DDL_Instance" runat="server" DataSourceID="SDS_Instance" DataTextField="Expr1" DataValueField="Id">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SDS_Instance" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT Id, site + ' ' + project AS Expr1 FROM instance"></asp:SqlDataSource>
    <asp:Button ID="b_role2user" runat="server" Text="Rolle und Instance den User zuweisen" OnClick="b_role2user_Click" />
    <br />
    <asp:SqlDataSource ID="SDS_roles" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT [id], [role] FROM [roles] ">
    </asp:SqlDataSource>
    <asp:GridView ID="gv_roles2user" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataSourceID="SDS_User2RoleList" AutoGenerateDeleteButton="True" DataKeyNames="idroles,iduser,id">
        <Columns>
            <asp:BoundField DataField="idroles" HeaderText="idroles" SortExpression="idroles" ReadOnly="True" />
            <asp:BoundField DataField="role" HeaderText="role" SortExpression="role" />
            <asp:BoundField DataField="iduser" HeaderText="iduser" SortExpression="iduser" ReadOnly="True" />
            <asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="site" HeaderText="site" SortExpression="site" />
            <asp:BoundField DataField="project" HeaderText="project" SortExpression="project" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SDS_User2RoleList" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>"  DeleteCommand="DELETE FROM [user2roles] WHERE [id] = @id" SelectCommand="SELECT * FROM [v_userroles] WHERE ([iduser] = @iduser)">
        <SelectParameters>
            <asp:ControlParameter ControlID="gv_users" Name="iduser" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDS_roles2user" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT role FROM roles"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" DeleteCommand="DELETE FROM [roles] WHERE [id] = @id" InsertCommand="INSERT INTO [roles] ([role], [roleDescription]) VALUES (@role, @roleDescription)" ProviderName="<%$ ConnectionStrings:ProCheckConnectionString1.ProviderName %>" SelectCommand="SELECT [id], [role], [roleDescription] FROM [roles]" UpdateCommand="UPDATE [roles] SET [role] = @role, [roleDescription] = @roleDescription WHERE [id] = @id">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="role" Type="String" />
            <asp:Parameter Name="roleDescription" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="role" Type="String" />
            <asp:Parameter Name="roleDescription" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
