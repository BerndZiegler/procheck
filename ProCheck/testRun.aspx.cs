﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;

namespace ProCheck
{
    public partial class testRun : System.Web.UI.Page
    {
        bsql xbsql = new bsql();
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void updategrid()
        {
            GridView1.DataBind();
            GV_openCases.DataBind();
            GV_bound_Cases.DataBind();
        }


     

        protected void GV_openCases_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            xbsql.sqlstatement(string.Format("insert into testCase2testRun (id_testCase,id_testRun) values ({0},{1})", Convert.ToInt32(GV_openCases.SelectedValue.ToString()), Convert.ToInt32(GridView1.SelectedValue.ToString())));
            updategrid();
        }

     

        protected void GV_bound_Cases_RowCommand1(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Remove", StringComparison.CurrentCultureIgnoreCase))
            {

                string id = e.CommandArgument.ToString();
                xbsql.sqlstatement(string.Format("delete from testCase2testRun where id={0} ",id));
                updategrid();
            }
        }

        protected void GV_openCases_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Add", StringComparison.CurrentCultureIgnoreCase))
            {
                string id = e.CommandArgument.ToString();
                xbsql.sqlstatement(string.Format("insert into testCase2testRun (id_testCase,id_testRun) values ({0},{1})", id, Convert.ToInt32(GridView1.SelectedValue.ToString())));
                updategrid();
            }
        }

        protected void bNewTestRun_Click(object sender, EventArgs e)
        {
            if (LB_Instance.SelectedIndex == -1)
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">alert(\"Bitte wählen sie eine Instanz\")</SCRIPT>");
            }
            else {  
                xbsql.sqlstatement(string.Format("insert into testRun (insert_ts, insert_user,id_instance) values ('{0}','{1}',{2})", DateTime.Now.ToString(), xauth.getNT(),Convert.ToInt32(LB_Instance.SelectedValue.ToString())));
                updategrid();
            }
        }
    }
}