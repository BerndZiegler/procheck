﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;
namespace ProCheck
{
    public partial class common : System.Web.UI.Page
    {
        bsql xbsql = new bsql();
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void updategrid()
        {
            GV_common.DataBind();
        }

        protected void B_newEntry_Click(object sender, EventArgs e)
        {
            xbsql.sqlstatement(string.Format("insert into common (insert_ts, insert_user) values ('{0}','{1}')", DateTime.Now.ToString(), xauth.getNT()));
            updategrid();
        }
    }
}