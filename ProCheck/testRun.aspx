﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="testRun.aspx.cs" Inherits="ProCheck.testRun" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Testlauf</h1>
    <hr />
    <h2>1. Instanz wählen und neuen Testlauf anlegen</h2>
    &nbsp;<asp:ListBox ID="LB_Instance" runat="server" DataSourceID="SDS_Instance" DataTextField="site" DataValueField="Id"></asp:ListBox>
    &nbsp;&nbsp;&nbsp;
    <asp:Button ID="bNewTestRun" runat="server" OnClick="bNewTestRun_Click" Text="Neuer TestRun" />
    <hr />
    <h2>2. Testlauf editieren oder wählen um Testcase anzufügen</h2>
    <asp:GridView ID="GridView1" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="Es sind keine Datensätze zum Anzeigen vorhanden.">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" ButtonType="Button" />
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="insert_ts" HeaderText="insert_ts" SortExpression="insert_ts" ReadOnly="True" />
            <asp:BoundField DataField="insert_user" HeaderText="insert_user" SortExpression="insert_user" ReadOnly="True" />
            <asp:BoundField DataField="change_ts" HeaderText="change_ts" SortExpression="change_ts" ReadOnly="True" />
            <asp:BoundField DataField="change_user" HeaderText="change_user" SortExpression="change_user" ReadOnly="True" />
            <asp:BoundField DataField="test_master" HeaderText="test_master" SortExpression="test_master" />
            <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
            <asp:BoundField DataField="started" HeaderText="started" SortExpression="started" />
            <asp:BoundField DataField="ended" HeaderText="ended" SortExpression="ended" ReadOnly="True" />
            <asp:BoundField DataField="result" HeaderText="result" SortExpression="result" ReadOnly="True" />
            <asp:BoundField DataField="status" HeaderText="status" SortExpression="status" ReadOnly="True" />
            <asp:BoundField DataField="site" HeaderText="site" SortExpression="site" ReadOnly="True" />
            <asp:BoundField DataField="project" HeaderText="project" SortExpression="project" ReadOnly="True" />
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" 
        DeleteCommand="DELETE FROM [testRun] WHERE [Id] = @Id" 
        InsertCommand="INSERT INTO [testRun] ([insert_ts], [insert_user], [change_ts], [change_user], [test_master], [name], [started], [ended], [result], [status]) VALUES (@insert_ts, @insert_user, @change_ts, @change_user, @test_master, @name, @started, @ended, @result, @status)" 
    
        SelectCommand="SELECT tr.[Id], tr.[insert_ts], tr.[insert_user], tr.[change_ts], tr.[change_user], [test_master], [name], [started], [ended], [result], [status], i.site,i.project FROM [testRun] as tr inner join instance i on i.id=tr.id_instance order by tr.id desc"
         UpdateCommand="UPDATE [testRun] SET [insert_ts] = @insert_ts, [insert_user] = @insert_user, [change_ts] = @change_ts, [change_user] = @change_user, [test_master] = @test_master, [name] = @name, [started] = @started, [ended] = @ended, [result] = @result, [status] = @status WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="insert_ts" Type="DateTime" />
            <asp:Parameter Name="insert_user" Type="String" />
            <asp:Parameter Name="change_ts" Type="DateTime" />
            <asp:Parameter Name="change_user" Type="String" />
            <asp:Parameter Name="test_master" Type="String" />
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="started" Type="DateTime" />
            <asp:Parameter Name="ended" Type="DateTime" />
            <asp:Parameter Name="result" Type="String" />
            <asp:Parameter Name="status" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="insert_ts" Type="DateTime" />
            <asp:Parameter Name="insert_user" Type="String" />
            <asp:Parameter Name="change_ts" Type="DateTime" />
            <asp:Parameter Name="change_user" Type="String" />
            <asp:Parameter Name="test_master" Type="String" />
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="started" Type="DateTime" />
            <asp:Parameter Name="ended" Type="DateTime" />
            <asp:Parameter Name="result" Type="String" />
            <asp:Parameter Name="status" Type="String" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    
<br />
    <table style="width:100%;">
      
        <tr>
            <td style="width:50%;">
                verwendete Testcase´s</td>
            <td style="width:50%;">
                verfügbare Testcases</td>
            <td>&nbsp;</td>
        </tr>
    
        <tr>
            <td style="width:50%;">
                <asp:GridView ID="GV_bound_Cases"  style="width:90%;vertical-align:top;align-self:center"  runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SDS_boundCases" OnRowCommand="GV_bound_Cases_RowCommand1">
                    <Columns>
                         <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LB2" runat="server" CommandName="remove" CommandArgument='<%#Eval("ID") %>'>Remove</asp:LinkButton>
                            </ItemTemplate>
                         </asp:TemplateField>

                        <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
                        <asp:BoundField DataField="titel" HeaderText="titel" SortExpression="titel" />
                        <asp:BoundField DataField="cluster" HeaderText="cluster" SortExpression="cluster" />
                    </Columns>
                    <EmptyDataTemplate>
                        Kein Testcase zugewiesen
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
            <td style="width:50%;">
                <asp:GridView ID="GV_openCases"  style="width:90%;vertical-align:top; align-self:center" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt"  DataKeyNames="id"  DataSourceID="SDS_openCases" OnRowCommand="GV_openCases_RowCommand" AutoGenerateColumns="False">
                    <Columns>
                         <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LB2" runat="server" CommandName="add" CommandArgument='<%#Eval("ID") %>'>Add</asp:LinkButton>
                            </ItemTemplate>
                         </asp:TemplateField>
                        <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                        <asp:BoundField DataField="titel" HeaderText="titel" SortExpression="titel" />
                        <asp:BoundField DataField="cluster" HeaderText="cluster" SortExpression="cluster" />
                        <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
                    </Columns>
                    <EmptyDataTemplate>
                        Kein Testcase für zuweisung verfügbar
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
            <td>&nbsp;</td>
        </tr>
    
    </table>
    <asp:SqlDataSource ID="SDS_Instance" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT [Id], [site], [project] FROM [instance]"></asp:SqlDataSource>
    <br />
    <asp:SqlDataSource ID="SDS_boundCases" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT Id, name, titel, cluster FROM v_testCase2testRun WHERE (id_testRun = @id_testRun)" DeleteCommand="DELETE FROM testCase2testRun WHERE id_testCase = @id_testCase AND id_testRun = @id_testRun">
        <DeleteParameters>
            <asp:Parameter Name="id_testCase" />
            <asp:Parameter Name="id_testRun" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id_testRun" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDS_openCases" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT id, titel, cluster, description FROM testCase WHERE id NOT IN (SELECT id_testCase FROM testCase2testRun where id_testRun = @id)">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
