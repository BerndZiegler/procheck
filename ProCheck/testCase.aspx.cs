﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;

namespace ProCheck
{
    public partial class testCase : System.Web.UI.Page
    {
        bsql xbsql = new bsql();
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public void updategrid()
        {
            GV_testCase.DataBind();
        }
        protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            updategrid();

        }
        public string ShowLineBreaks(object text)
        {
            return (text.ToString().Replace("\n", "<br/>"));
        }

        protected void dv_testCaseDetail_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            updategrid();
        }

        protected void dv_testCaseDetail_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
        {
            updategrid();
        }

        protected void B_newCase_Click(object sender, EventArgs e)
        {
            xbsql.sqlstatement(string.Format("insert into testCase (insert_ts, insert_user) values ('{0}','{1}')", DateTime.Now.ToString(), xauth.getNT()));
            updategrid();
        }

        protected void dv_testCaseDetail_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {

        }


    }
}