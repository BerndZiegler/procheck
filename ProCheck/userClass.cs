﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace ProCheck
{ 
      
    class userClass
    {
        saus2.bsql xbsql = new saus2.bsql();
        saus2.auth xauth = new saus2.auth();
       
        public string email { get; private set; }
        public string name { get; private set; }
        public string logonname { get; set; }

        public int userid { get; private set; }
        public void BuildUser()
        {
           string temp = xbsql.sqlstatementget(string.Format("select isnull(username,'noname')+';'+isnull(email,'nomail@cde-bs.local')+';'+isnull(logon,'noLogon') from users where id = {0}",xauth.getUserID() ));
            
           string[] tempx = temp.Split(';');
           
           name = tempx[0];
           email = tempx[1];
           logonname = tempx[2];
        
        }
       
    }
}