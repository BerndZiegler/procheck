﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Der Code wurde von einem Tool generiert.
//
//     Änderungen an der Datei führen möglicherweise zu falschem Verhalten, und sie gehen verloren, wenn
//     der Code erneut generiert wird. 
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace ProCheck {
    
    
    public partial class issueUser {
        
        /// <summary>
        /// tb_User-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_User;
        
        /// <summary>
        /// LinkButton1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton1;
        
        /// <summary>
        /// PanelNeuerIssue-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelNeuerIssue;
        
        /// <summary>
        /// DDL_Instance-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDL_Instance;
        
        /// <summary>
        /// SDS_Instance-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_Instance;
        
        /// <summary>
        /// DDL_Type-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDL_Type;
        
        /// <summary>
        /// SDS_common-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_common;
        
        /// <summary>
        /// tb_titel-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_titel;
        
        /// <summary>
        /// tb_description-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_description;
        
        /// <summary>
        /// DDL_Prio-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DDL_Prio;
        
        /// <summary>
        /// SDS_IssuePrio-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_IssuePrio;
        
        /// <summary>
        /// b_IssueSave-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button b_IssueSave;
        
        /// <summary>
        /// GV_OpenIssues-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GV_OpenIssues;
        
        /// <summary>
        /// SDS_openIssues-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_openIssues;
        
        /// <summary>
        /// lb_NewRemark-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton lb_NewRemark;
        
        /// <summary>
        /// P_NewRemark-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel P_NewRemark;
        
        /// <summary>
        /// tb_Comment-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_Comment;
        
        /// <summary>
        /// RBL_info-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList RBL_info;
        
        /// <summary>
        /// b_coment-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button b_coment;
        
        /// <summary>
        /// GV_Comments-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GV_Comments;
        
        /// <summary>
        /// SDS_IssueRemarks-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_IssueRemarks;
        
        /// <summary>
        /// tb_search-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tb_search;
        
        /// <summary>
        /// b_searchClosed-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button b_searchClosed;
        
        /// <summary>
        /// GV_closedIssues-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GV_closedIssues;
        
        /// <summary>
        /// SDS_closedIssues-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_closedIssues;
        
        /// <summary>
        /// gv_closedIssuesRemark-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gv_closedIssuesRemark;
        
        /// <summary>
        /// SDS_closedIssuesRemark-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Um dies zu ändern, verschieben Sie die Felddeklaration aus der Designerdatei in eine Code-Behind-Datei.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource SDS_closedIssuesRemark;
    }
}
