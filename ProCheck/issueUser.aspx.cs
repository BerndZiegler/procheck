﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using saus2;
using System.Configuration;
using System.IO;


namespace ProCheck
{
    public partial class issueUser : System.Web.UI.Page
    {
        string secoundLevelMail = ConfigurationManager.AppSettings["secoundLevelMail"].ToString();
        string thirdLevelMail = ConfigurationManager.AppSettings["thirdLevelMail"].ToString();
        string templatefilepath = ConfigurationManager.AppSettings["templatefilepath"].ToString();
        string ownermail;

        userClass xuserclass = new userClass();
                    
        bmail xbmail = new bmail();
        bsql xbsql = new bsql();
        auth xauth = new auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            tb_User.Text = xauth.getNT();
            xuserclass.BuildUser();
        }
        public void updategrid()
        {
            GV_OpenIssues.DataBind();
            GV_Comments.DataBind();
            GV_closedIssues.DataBind();
           
        }
        protected void b_IssueSave_Click(object sender, EventArgs e)
        {
            try
            {

            ownermail = xuserclass.email.ToString();
            xbsql.sqlstatement(string.Format("insert into issue (insert_ts, insert_user,id_instance,type,titel,description,prio,status) values ('{0}','{1}',{2},'{3}','{4}','{5}','{6}','{7}')", 
                DateTime.Now.ToString(), xauth.getNT(),DDL_Instance.SelectedValue,DDL_Type.SelectedValue.ToString(),tb_titel.Text,tb_description.Text,DDL_Prio.SelectedValue,"open"));
            updategrid();
            PanelNeuerIssue.Visible = false;

         //   string MailHeader =   generateTemplate(string.Format("{0}|{1}|{2}", xuserclass.name, tb_titel.Text.ToString(), tb_description.Text.ToString()),templatefilepath+"I_MailHeader_New.txt");
           // string MailBody = generateTemplate(string.Format("{0}|{1}|{2}", xuserclass.name, tb_titel.Text.ToString(), tb_description.Text.ToString()), templatefilepath + "I_MailBody_New.txt");

            string MailHeader = generateTemplate( xuserclass.name, tb_titel.Text.ToString(), tb_description.Text.ToString(), templatefilepath + "I_MailHeader_New.txt","new");
            string MailBody = generateTemplate( xuserclass.name, tb_titel.Text.ToString(), tb_description.Text.ToString(), templatefilepath + "I_MailBody_New.txt","new");
        
                xbmail.sendeMail(secoundLevelMail + ";" + xuserclass.email.ToString(),MailHeader , MailBody);
             
            //    xbmail.sendeMail(secoundLevelMail + ";" + xuserclass.email.ToString(), string.Format("Neuer Eintrag in ProCheck von {0}   : {1} " , xuserclass.name, tb_titel.Text.ToString()), tb_description.Text.ToString());
                PanelNeuerIssue.Visible = false;   
                
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert",string.Format( "alert('Fehler beim Anlegen eines Issues: {0} ')",ex.Message), true);
            
            }
        }
        public string generateTemplate(string user,string titel, string description, string Templatename,string status)
        {
            // value sind die werte 0 bis x welche im template ersetzt werden mit ; getrennt
            var template = File.ReadAllText(Templatename);
           
          
                template = template.Replace("$1$", user);
                template = template.Replace("$2$", titel);
                template = template.Replace("$3$", description);
                template = template.Replace("$4$", status);

        
            return template;
        }       

        public string generateTemplate(string val, string Templatename)
        {          
            // value sind die werte 0 bis x welche im template ersetzt werden mit ; getrennt
            var template = File.ReadAllText(Templatename);
                var values = val.Split(';');
                int valuecount = 1;
               foreach (var value in values)
                {
                    template = template.Replace("$" + valuecount.ToString(), value);
                    valuecount++;
                }
                return template;
        }       

        protected void b_coment_Click(object sender, EventArgs e)
        {
            try
            {

                string status = RBL_info.Text;
                string IssueID = GV_OpenIssues.SelectedDataKey.Value.ToString();
                   
        // Status ermitteln
                 switch (status)
                {
                      case "cancel":
                        // Es soll Inhalt des textfeldes gespeichert wwerden und der Status des Issues auf "cancel" gesetzt werden und eine email an Ersteller und Admin gesendet werden
                        xbsql.sqlstatement(string.Format("update issue set status='{0}' where id={1}", status, IssueID));
                       break;

                    case "comment":
                        break;

                    case "solved":
                        xbsql.sqlstatement(string.Format("update issue set status='{0}',endtime=SYSDATETIME()   where id={1}", status, IssueID));                        
                        break;

                    case "waitForUser":
                        xbsql.sqlstatement(string.Format("update issue set status='{0}' where id={1}", status, IssueID));  
                        break;

                    case "develop":
                        xbsql.sqlstatement(string.Format("update issue set status='{0}' where id={1}", status, IssueID));                        
                        break;

                    case "noSelection":
                        break;

                    default:
                        status = "noSelection";
                        break;

                }

        // Mail erstellen
                if (status != "noSelection")
                {
                    string userNT = xauth.getNT().ToString();
                    string userMail = xauth.getInfo("email");

                    string issueOwnerMail = xbsql.sqlstatementget(string.Format("select email from users where logon=(select insert_user from issue  where id={0}) ", IssueID));
                    
                    //outdatet             string currentEmail = xauth.getUserInfo(xauth.getNT().ToString(), "email");
                    

                    string meldungen = xbsql.sqlstatementget("select titel + '|' + description from issue where id=" + IssueID);

                        string[] meldungenarr = meldungen.Split('|');

                        string MeldungHeader = string.Format("ProCheck - Titel: {0} - Status: {1} - User: {2}", meldungenarr[0], status, userNT);                    
                    string Meldung = string.Format("{0} <br> Inhalt: <br>{1}", MeldungHeader, tb_Comment.Text);
                    xbsql.sqlstatement(string.Format("insert into issueComment (insert_ts, insert_user,id_issue,description) values ('{0}','{1}',{2},'{3}')",
                    DateTime.Now.ToString(), userNT, IssueID, Meldung.Replace("'", "''")));
                    updategrid();

                    //   string MailHeader = generateTemplate(string.Format("{0};{1};{2};{3}", meldungenarr[0], status, userNT, tb_Comment.Text), templatefilepath + "I_MailHeader_Comment.txt");
                    // string MailBody = generateTemplate(string.Format("{0};{1};{2};{3}", meldungenarr[0], status, userNT, tb_Comment.Text), templatefilepath + "I_MailBody_Comment.txt");
        //        string MailHeader = generateTemplate(string.Format("{0};{1};{2};{3}", meldungenarr[0], status, userNT, tb_Comment.Text), templatefilepath + "I_MailHeader_Comment.txt");
          //      string MailBody = generateTemplate(string.Format("{0};{1};{2};{3}", meldungenarr[0], status, userNT, tb_Comment.Text), templatefilepath + "I_MailBody_Comment.txt");
                string MailHeader = generateTemplate(meldungenarr[0], userNT, tb_Comment.Text, templatefilepath + "I_MailHeader_Comment.txt", status);
                string MailBody = generateTemplate( meldungenarr[0], userNT, tb_Comment.Text, templatefilepath + "I_MailBody_Comment.txt", status);
          
                    xbmail.sendeMail(secoundLevelMail + ";" + userMail + ";" + issueOwnerMail, MailHeader, MailBody);
                    tb_Comment.Text = "";
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Es ist kein Issue oder kein Status gewählt!')", true);            
                }
            }
            catch (Exception exx)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Fehler:" + exx.Message + "')", true);
            }
            P_NewRemark.Visible = false;
        }

        protected void GV_OpenIssues_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GV_OpenIssues.SelectedRowStyle.BackColor = System.Drawing.Color.Yellow;
            P_NewRemark.Visible = true;
      
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (PanelNeuerIssue.Visible == false)
                PanelNeuerIssue.Visible = true;
            else
                PanelNeuerIssue.Visible = false;

        }
        protected void lb_NewRemark_Click(object sender, EventArgs e)
        {
            if (P_NewRemark.Visible == false)
            P_NewRemark.Visible = true;
            else
                P_NewRemark.Visible = false;
        }

      
        protected void GV_closedIssues_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch ( e.CommandName)
            {
                case "restart":
                     GridViewRow SelectedRow = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                     var selid = SelectedRow.Cells[2].Text;

                    xbsql.sqlstatement(string.Format("update issue set status='restart' where id={0} ",selid));
                    xbsql.sqlstatement(string.Format("insert into issueComment (insert_ts, insert_user,id_issue,description) values ('{0}','{1}',{2},'{3}')",
                   DateTime.Now.ToString(), xauth.getNT(), selid , xauth.getNT()+ " restart Issue "));
                   
                    updategrid();
                    break;
                default:

                    GV_closedIssues.SelectedRowStyle.BackColor = System.Drawing.Color.Yellow;
                    break;
            }
           
        }


        protected void b_searchClosed_Click(object sender, EventArgs e)
        {
            if (tb_search.Text != "")
                SDS_closedIssues.FilterExpression = string.Format("titel like '%{0}%' OR description like  '%{0}%'", tb_search.Text);
            else
                SDS_closedIssues.FilterExpression = null;
            SetFocus(tb_search);
        }
    }
}