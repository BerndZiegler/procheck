﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="common.aspx.cs" Inherits="ProCheck.common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Wertetabellen Verwalten</h1>
    <h2>1. Neuen Eintrag anlegen</h2>
        <asp:Button ID="B_newEntry" runat="server" OnClick="B_newEntry_Click" Text="neuer Eintrag" />
    <hr />
    <h2>2. Eintrag bearbeiten</h2>
    <asp:GridView ID="GV_common" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" EmptyDataText="Es sind keine Datensätze zum Anzeigen vorhanden.">
    <Columns>
        <asp:CommandField ShowEditButton="True" />
        <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" />
        <asp:BoundField DataField="cluster" HeaderText="cluster" SortExpression="cluster" />
        <asp:BoundField DataField="value" HeaderText="value" SortExpression="value" />
        <asp:BoundField DataField="description" HeaderText="description" SortExpression="description" />
    </Columns>
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" DeleteCommand="DELETE FROM [common] WHERE [id] = @id" InsertCommand="INSERT INTO [common] ([cluster], [value], [description]) VALUES (@cluster, @value, @description)" ProviderName="<%$ ConnectionStrings:ProCheckConnectionString1.ProviderName %>" SelectCommand="SELECT [id], [cluster], [value], [description] FROM [common]" UpdateCommand="UPDATE [common] SET [cluster] = @cluster, [value] = @value, [description] = @description WHERE [id] = @id">
    <DeleteParameters>
        <asp:Parameter Name="id" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="cluster" Type="String" />
        <asp:Parameter Name="value" Type="String" />
        <asp:Parameter Name="description" Type="String" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="cluste" Type="String" />
        <asp:Parameter Name="value" Type="String" />
        <asp:Parameter Name="description" Type="String" />
        <asp:Parameter Name="id" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
</asp:Content>
