﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="testCase.aspx.cs" Inherits="ProCheck.testCase" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Testcases</h1>
    <hr />
    <h2>1. Testcase anlegen</h2>
    <asp:Button ID="B_newCase" runat="server" OnClick="B_newCase_Click" Text="Neuer Testcase" />
    <hr />
     <h2>2. Testcase wählen und bearbeiten.</h2>
    <asp:GridView ID="GV_testCase" runat="server" CssClass="Grid" AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SDS_testCases">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Button" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="insert_ts" HeaderText="insert_ts" SortExpression="insert_ts" Visible="False" />
            <asp:BoundField DataField="insert_user" HeaderText="insert_user" SortExpression="insert_user" Visible="False" />
            <asp:BoundField DataField="change_ts" HeaderText="change_ts" SortExpression="change_ts" Visible="False" />
            <asp:BoundField DataField="change_user" HeaderText="change_user" SortExpression="change_user" Visible="False" />
            <asp:BoundField DataField="titel" HeaderText="titel" SortExpression="titel" />
            <asp:BoundField DataField="cluster" HeaderText="cluster" SortExpression="cluster" />
            <asp:TemplateField HeaderText="description">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# ShowLineBreaks(Eval("description")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="expect_result">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# ShowLineBreaks(Eval("expect_result")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SDS_testCases" runat="server" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" SelectCommand="SELECT * FROM [testCase] ORDER BY [id] DESC"></asp:SqlDataSource>
    
    Detail<br />
    <asp:DetailsView ID="dv_testCaseDetail" runat="server" AutoGenerateRows="False" DataKeyNames="id" DataSourceID="SDS_testCaseDetail" Height="50px" OnItemUpdated="DetailsView1_ItemUpdated" Width="100%" OnItemDeleted="dv_testCaseDetail_ItemDeleted" OnItemInserted="dv_testCaseDetail_ItemInserted" OnItemUpdating="dv_testCaseDetail_ItemUpdating">
        <EmptyDataTemplate>
            Bitte Testcase wählen
        </EmptyDataTemplate>
        <Fields>
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="insert_ts" HeaderText="insert_ts" SortExpression="insert_ts" ReadOnly="True" />
            <asp:BoundField DataField="insert_user" HeaderText="insert_user" SortExpression="insert_user" ReadOnly="True" />
            <asp:BoundField DataField="change_ts" HeaderText="change_ts" SortExpression="change_ts" ReadOnly="True" />
            <asp:BoundField DataField="change_user" HeaderText="change_user" SortExpression="change_user" ReadOnly="True" />
            <asp:TemplateField HeaderText="titel" SortExpression="titel">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("titel") %>'  TextMode="MultiLine" Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("titel") %>' TextMode="MultiLine" Width="100%"></asp:Label>
                </ItemTemplate>
                <ItemStyle Wrap="False" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="cluster" SortExpression="cluster">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("cluster") %>' TextMode="MultiLine" Rows="5" Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("cluster") %>' TextMode="MultiLine" Width="100%"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="description" SortExpression="description">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("description") %>' TextMode="MultiLine" Rows="5" Width="100%"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# ShowLineBreaks(Eval("description")) %>' TextMode="MultiLine" Width="100%"></asp:Label>   
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="expect_result" SortExpression="expect_result">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("expect_result") %>' TextMode="MultiLine" Rows="5" Width="100%"></asp:TextBox>
                </EditItemTemplate>          
                <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# ShowLineBreaks(Eval("expect_result")) %>' TextMode="MultiLine" Width="100%"></asp:Label>               
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ButtonType="Button" />
        </Fields>
    </asp:DetailsView>
    <asp:SqlDataSource ID="SDS_testCaseDetail" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:ProCheckConnectionString1 %>" 
        OldValuesParameterFormatString="original_{0}" 
        SelectCommand="SELECT * FROM [testCase] WHERE ([id] = @id)" 
        UpdateCommand="UPDATE [testCase] SET [change_ts] = @change_ts, [change_user] = @change_user, [titel] = @titel, [cluster] = @cluster, [description] = @description, [expect_result] = @expect_result WHERE [id] = @original_id AND (([insert_ts] = @original_insert_ts) OR ([insert_ts] IS NULL AND @original_insert_ts IS NULL)) AND (([insert_user] = @original_insert_user) OR ([insert_user] IS NULL AND @original_insert_user IS NULL)) AND (([change_ts] = @original_change_ts) OR ([change_ts] IS NULL AND @original_change_ts IS NULL)) AND (([change_user] = @original_change_user) OR ([change_user] IS NULL AND @original_change_user IS NULL)) AND (([titel] = @original_titel) OR ([titel] IS NULL AND @original_titel IS NULL)) AND (([cluster] = @original_cluster) OR ([cluster] IS NULL AND @original_cluster IS NULL)) AND (([description] = @original_description) OR ([description] IS NULL AND @original_description IS NULL)) AND (([expect_result] = @original_expect_result) OR ([expect_result] IS NULL AND @original_expect_result IS NULL))">
         <SelectParameters>
            <asp:ControlParameter ControlID="GV_testCase" Name="id" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="insert_ts" Type="DateTime" />
            <asp:Parameter Name="insert_user" Type="String" />
            <asp:Parameter Name="change_ts" Type="DateTime" />
            <asp:Parameter Name="change_user" Type="String" />
            <asp:Parameter Name="titel" Type="String" />
            <asp:Parameter Name="cluster" Type="String" />
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="expect_result" Type="String" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_insert_ts" Type="DateTime" />
            <asp:Parameter Name="original_insert_user" Type="String" />
            <asp:Parameter Name="original_change_ts" Type="DateTime" />
            <asp:Parameter Name="original_change_user" Type="String" />
            <asp:Parameter Name="original_titel" Type="String" />
            <asp:Parameter Name="original_cluster" Type="String" />
            <asp:Parameter Name="original_description" Type="String" />
            <asp:Parameter Name="original_expect_result" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>
