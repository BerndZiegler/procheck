﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace saus2
{

    public class bsql
    {
        string connstr = ConfigurationManager.ConnectionStrings["ProCheckConnectionString1"].ConnectionString;
        public void sqlstatement(String Statement)
        {
            try
            {
                // string connstr = ConfigurationManager.ConnectionStrings["ProCheckConnectionString1"].ConnectionString;
                SqlConnection con = new SqlConnection(connstr);
                string strSQL = Statement;
              
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                //NEW
                cmd.Connection.Close();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    

        public String sqlstatementget(String Statement)
        {
          //  String connstr = System.Configuration.ConfigurationManager.ConnectionStrings["sausConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connstr);
            string strSQL = Statement;
            SqlCommand cmd = new SqlCommand(strSQL, con);
            con.Open();
            if (cmd.ExecuteScalar() == null)
            {
                return "";
            }
            else
            {
                return (string)cmd.ExecuteScalar().ToString();
            }
        }

        public DataSet  sqlstatementgetandmail(String Statement)
        {
            bmail xbmail = new bmail();

          //  String connstr = System.Configuration.ConfigurationManager.ConnectionStrings["sausConnectionString"].ConnectionString;

                   using (SqlConnection cnn = new SqlConnection(connstr))  {

                    SqlDataAdapter da = new SqlDataAdapter(Statement, cnn);

                    DataSet ds = new DataSet();
                    da.Fill(ds, "email");
                    return ds;


                   }


     
            
        }

        public int insert(string table, string column, string value)
        {
         //   string connstr = System.Configuration.ConfigurationManager.ConnectionStrings["sausConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connstr);
            string strSQL = "insert into "+table+" ("+column+") values ('" +value+"')" ;
            SqlCommand cmd = new SqlCommand(strSQL, con);         
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            //NEW
            cmd.Connection.Close();

            strSQL = "select top 1  id  from " + table + " order by id desc ";
            cmd = new SqlCommand(strSQL, con);
            con.Open();
            int anfrage = (int)cmd.ExecuteScalar();
            return anfrage;
        }

        public String read(string table, string getcolumn,  string searchcolumn, string value)
        {
         //   string connstr = System.Configuration.ConfigurationManager.ConnectionStrings["sausConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(connstr);
            string strSQL = "select " + getcolumn + " from " + table + " where " + searchcolumn + " = " +value ;
            SqlCommand cmd = new SqlCommand(strSQL, con);
            con.Open();
            String anfrage = (string) cmd.ExecuteScalar();
            

            return anfrage;
        }


    }
}